package middleware

import (
	"net/http"
	"strings"
)

func jwtMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		header := r.Header.Get("Authorization")
		jwt := strings.Split(header, " ")[1]
		if validate(jwt) {
			next.ServeHTTP(w, r)
		} else {
			w.WriteHeader(http.StatusUnauthorized)
		}
	})
}

func validate(wjt string) bool {
	return true
}
