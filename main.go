package main

import (
	"fmt"
	"net/http"

	"bitbucket.org/rwirdemann/rest-apis-go/external/mysql"
	"bitbucket.org/rwirdemann/rest-apis-go/handler"
)

func main() {
	repository := mysql.NewMySqlRepository()
	fmt.Printf("Server started on port 8080...\n")
	http.ListenAndServe(":8080", handler.NewRouter(repository))
}
