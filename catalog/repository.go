package catalog

type Repository interface {
	AddProduct(p Product) int
	AllProducts() []Product
	FindProducts(q string) []Product
	ProductById(id int) (Product, bool)
	Delete(id int) bool
	Contains(name string) bool
	Update(id int, p Product) bool
}

type DefaultRepository struct {
	products []Product
}

func (r *DefaultRepository) AddProduct(p Product) int {
	r.products = append(r.products, p)
	return 0
}

func (r *DefaultRepository) AllProducts() []Product {
	return r.products
}

func (r *DefaultRepository) FindProducts(q string) []Product {
	return r.products
}

func (r *DefaultRepository) ProductById(id int) (Product, bool) {
	for _, p := range r.products {
		if p.Id == id {
			return p, true
		}
	}

	return Product{}, false
}

func (r *DefaultRepository) Delete(id int) bool {
	if i := index(id, r.products); i != -1 {
		r.products = append(r.products[:i], r.products[i+1:]...)
		return true
	}

	return false
}

func index(id int, a []Product) int {
	for i, p := range a {
		if p.Id == id {
			return i
		}
	}
	return -1
}

func (r *DefaultRepository) Contains(name string) bool {
	for _, p := range r.AllProducts() {
		if p.Name == name {
			return true
		}
	}
	return false
}

func (r *DefaultRepository) Update(id int, p Product) bool {
	if i := index(id, r.products); i != -1 {
		r.products[i] = p
		return true
	}

	return false
}
