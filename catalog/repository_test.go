package catalog

import "testing"

func TestAddProduct(t *testing.T) {

	// Setup
	r := DefaultRepository{}

	// Method under test
	p := Product{Name: "Schuhe"}
	r.AddProduct(p)

	// Assertions
	if len(r.AllProducts()) != 1 {
		t.Fatalf("wanted %d, got %d", 1, len(r.AllProducts()))
	}
}
