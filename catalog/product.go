package catalog

type Product struct {
	Id          int
	Name        string
	Description string
	Category    string
	Price       float32
}
