USE catalog;
DROP TABLE IF EXISTS products;

CREATE TABLE products (
    id          INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT, 
    name        VARCHAR(50), 
	description VARCHAR(1000),
    category    VARCHAR(20),
    price       DECIMAL(8,2)
);

INSERT INTO products (name, description, category, price) VALUES ('Schuhe', "1460 Robust", "Kleidung", 179.0);
INSERT INTO products (name, description, category, price) VALUES ('Jacke', "Wasserdicht", "Kleidung", 299.0);
