package mysql

import (
	"database/sql"
	"fmt"

	"bitbucket.org/rwirdemann/rest-apis-go/catalog"
	_ "github.com/go-sql-driver/mysql"
)

type MySqlRepository struct {
	db        *sql.DB
	Connected bool
}

func NewMySqlRepository() *MySqlRepository {
	r := MySqlRepository{}
	r.connect("ralf", "Start1234", "catalog")
	return &r
}

func (r *MySqlRepository) connect(user string, password string, databaseName string) {
	dataSourceName := fmt.Sprintf("%s:%s@/%s?parseTime=true", user, password, databaseName)
	var err error
	if r.db, err = sql.Open("mysql", dataSourceName); err != nil {
		panic(err)
	}

	if err = r.db.Ping(); err != nil {
		panic(err)
	}

	r.Connected = true
}

func (r *MySqlRepository) AddProduct(p catalog.Product) int {
	return -1
}

func (r *MySqlRepository) AllProducts() []catalog.Product {
	rows, err := r.db.Query("select id, name, description, category, price from products")
	if err != nil {
		panic(err)
	}

	var products []catalog.Product
	for rows.Next() {
		var id int
		var name, description, category string
		var price float32
		if err := rows.Scan(&id, &name, &description, &category, &price); err != nil {
			panic(err)
		} else {
			products = append(products, catalog.Product{Id: id, Name: name, Description: description, Category: category, Price: price})
		}
	}

	return products
}

func (r *MySqlRepository) FindProducts(q string) []catalog.Product {
	return []catalog.Product{}
}

func (r *MySqlRepository) ProductById(id int) (catalog.Product, bool) {
	return catalog.Product{}, false
}

func (r *MySqlRepository) Delete(id int) bool {
	return false
}

func (r *MySqlRepository) Contains(name string) bool {
	return false
}

func (r *MySqlRepository) Update(id int, p catalog.Product) bool {
	return false
}
